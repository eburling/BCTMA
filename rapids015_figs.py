from tqdm.notebook import tqdm
from scipy.spatial import ConvexHull
import seaborn as sns
import holoviews as hv
import colorcet
import pandas as pd
import pingouin as pg
from scipy.stats import bartlett
from util import get_dendrogram_short, compute_linkage

from scipy.stats import entropy
from upsetplot import from_memberships,plot
from itertools import compress
import matplotlib.pyplot as mpl

import numpy as np
import cudf
import cupy as cp
from cuml.neighbors import NearestNeighbors

import matplotlib.pyplot as plt
import matplotlib as mpl

import cuml
import networkx as nx

hv.extension('bokeh')

def generate_fig4(df):
    thresh = 1
    df['CK+'] = ((df[['Enorm_scene_CK8_Ring', 
                     'Enorm_scene_CK19_Ring', 
                     'Enorm_scene_CK7_Ring', 
                     'Enorm_scene_CK5_Ring',
                     'Enorm_scene_CK14_Ring']]>thresh).any(axis=1) )#& (df['Enorm_scene_Ecad_Ring']>thresh))

    for stain in ['Enorm_scene_CK8_Ring', 
                  'Enorm_scene_CK17_Ring', 
                  'Enorm_scene_CK19_Ring', 
                  'Enorm_scene_CK7_Ring', 
                  'Enorm_scene_CK5_Ring',
                  'Enorm_scene_CK14_Ring',
                  'Enorm_scene_Vim_Ring',
                  'Enorm_scene_CD44_Ring']:

        name = f'{stain.split("_")[2]}+'
        df[name] = df[stain]>thresh

    cks = ['CK7+','CK8+','CK5+','CK14+','CK19+']
    all_combos = df.groupby(cks).size().reset_index(name='count')[cks]
    data = []
    for source in df.source.unique():
        source_df = df[df.source==source]
        for scene in source_df.scene.unique():
            scene_df = source_df[(source_df.scene==scene)&\
                                 (source_df.metacluster_name!='Segmentation error')&\
                                 (source_df['CK+']==True)]
            subtype = scene_df.clinical_subtype.iloc[0]
            counts = scene_df.groupby(cks).size().reset_index(name='Count')
            missing_combos = pd.concat([all_combos, counts[cks], counts[cks]]).drop_duplicates(keep=False)
            missing_combos['Count'] = 0
            counts = pd.concat([counts,missing_combos],ignore_index=True)
            counts = counts[counts.any(axis=1)]
    #         s = entropy(softmax(np.arcsinh(counts.Count)))
            s = entropy(counts.Count/counts.Count.sum(), base=2)
            data.append((subtype,s))

    s_df = pd.DataFrame(data,columns=['clinical subtype','entropy'])
    s_df['clinical subtype'] = pd.Categorical(s_df['clinical subtype'], 
                                              categories=["TN","HR+HER2-","HR-HER2+","HR+HER2+"],
                                              ordered=True)
    s_df.sort_values(by='clinical subtype',inplace=True)

    def hook(plot, element):
        plot.handles['xaxis'].axis_label_text_font = 'helvetica'

    fig = hv.BoxWhisker(s_df,
                  kdims=['clinical subtype'],
                  vdims='entropy').opts(ylabel='CK heterogeneity (entropy in bits)',
                                        box_fill_color=hv.dim('clinical subtype').str(),
                                        cmap=['#b3b3b3','#e5c494','#ffd92f','#a6d854'],
                                        toolbar=None,
                                        hooks=[hook]).redim.range(entropy=(0.5,5))

    print('homogeneity of variances met, using ANOVA and Tukey post-hoc test')
    bartlett(s_df[s_df['clinical subtype']=='HR+HER2-'].entropy,
             s_df[s_df['clinical subtype']=='TN'].entropy,
             s_df[s_df['clinical subtype']=='HR-HER2+'].entropy,
             s_df[s_df['clinical subtype']=='HR+HER2+'].entropy)

    print(pg.pairwise_tukey(data=s_df, dv='entropy',
                            between='clinical subtype').round(10))
    
    
    st_counts = df[df['CK+']==True].groupby(['clinical_subtype']+cks).size().reset_index(name='Count')
    font = {'family': 'sans-serif',
            'size': 5}
    mpl.rc('font', **font)
    for st in [['HR+HER2-','#e5c494'],
               ['HR-HER2+', '#ffd92f'],
               ['HR+HER2+', '#a6d854'],
               ['TN', '#b3b3b3']]:

        cell_types = []
        for i in st_counts[st_counts.clinical_subtype==st[0]].iterrows():
            cell_types.append(list(compress(cks,i[1][cks].values)))

        us = from_memberships(cell_types,st_counts[st_counts.clinical_subtype==st[0]].Count).reorder_levels([1,4,3,2,0])
        plot(us,show_percentages=True,facecolor=st[1],sort_categories_by=None,show_counts=True,orientation='horizontal')
        
    return fig
    
    
def generate_fig5ac(df):

    df = df[df.metacluster_name!='Segmentation error']
    # df.clinical_subtype = df.clinical_subtype.map({'ER':'HR+HER2-','HER2':'HR-HER2+','HER2/ER':'HR+HER2+','TN':'TN'})
    df['source_scene'] = df.source + '_' + df.scene.astype(str)

    immune_clusters = [0,1,3,4,5,6]
    stroma_clusters = [6,8,9,10]
    endoth_clusters = [2,11]
    apopto_clusters = [7]
    cancer_clusters = [12,13,14,15,16,17,18,19,22,23,24,25,26]
    prolif_clusters = [20,21]

    map_dict = {
        **dict.fromkeys(cancer_clusters+apopto_clusters+prolif_clusters, 0),
        **dict.fromkeys(immune_clusters, 1),
        **dict.fromkeys(stroma_clusters+endoth_clusters, 2)
    }

    df['group'] = df.metacluster.map(map_dict)

    factor = 0.325

    scale_factor = 0.325*1e-3 # mm/px
    density_dfs = []
    for source in tqdm(df.source.unique()):
        source_df = df[df.source==source]
        for scene in tqdm(source_df.scene.unique()):
            scene_df = source_df[(source_df.scene==scene)]
            hull = ConvexHull(scene_df[['centroid_x','centroid_y']]*scale_factor)
            density_df = (scene_df.groupby(['metacluster_name','metacluster','clinical_subtype']).size() / hull.volume).reset_index().rename(columns={0:'cell density'})
            density_df['source_scene'] = source + '_' + str(scene)
            density_dfs.append(density_df)

    colors = [
        '#008000', 
        '#006400', 
        '#FFFF00', 
        '#808000', 
        '#7CFC00',
        '#556B2F',
        '#00BFFF',
        '#0000CD', 
        '#87CEFA',
        '#B0E0E6',
        '#5F9EA0',
        '#CCCC00', 
        '#FA8072',
        '#CD5C5C',
        '#FF0000',
        '#800000',
        '#FF8C00',
        '#FFC0CB',
        '#DC143C',
        '#9400D3',
        '#20B2AA',
        '#008080',
        '#DB7093',
        '#CD853F',
        '#FF69B4',
        '#FF1493',
        '#A0522D'
            ]

    name_mapper = df[['metacluster','metacluster_name']].drop_duplicates().sort_values(by='metacluster')
    cmap = dict(list(zip(name_mapper.metacluster_name.tolist(),colors)))

    df.loc[df.grade=='-','grade'] = 0
    df.loc[df.grade=='None','grade'] = 0
    df.grade = df.grade.astype('float').astype(int).astype('category')

    df.loc[df.stage=='None','stage'] = 0
    df.stage = df.stage.astype('category')

    df['source_scene'] = (df['source'] + '_' + df['scene'].astype(str)).astype('category')

    meta_df = df[['source_scene','grade','stage','clinical_subtype']].drop_duplicates()

    data = pd.concat(density_dfs).sort_values(['clinical_subtype','metacluster'])

    # specify which cell metacluster density to cluster on by using e.g. .isin(stroma_clusters+endoth_clusters)
    g = sns.clustermap(data.pivot(index='source_scene',columns='metacluster_name',values='cell density').fillna(0),
                   col_cluster=False,
                   method='ward',
                      z_score=1)

    meta_df.source_scene.cat.set_categories(g.data2d.index,inplace=True)  # Set category sorting
    meta_df = meta_df.sort_values('source_scene')

    melted = pd.melt(g.data2d.reset_index(),
                     id_vars='source_scene',
                     var_name='metacluster_name',
                     value_name='cell density')

    hm = hv.HeatMap(melted,kdims=['metacluster_name','source_scene']).opts(width=700,
                                                              height=500,
                                                              border=0,
                                                              tools=['hover'],
                                                              colorbar=True,
                                                              colorbar_position='bottom',
                                                              cmap='Spectral_r',
                                                              symmetric=True)

    dendros = get_dendrogram_short(hm, ['source_scene','metacluster_name'], 'cell density')

    data.source_scene = data.source_scene.astype('category')      # Make categorical for sorting
    data.source_scene.cat.set_categories(g.data2d.index,inplace=True)  # Set category sorting
    data = data.sort_values(['source_scene','metacluster'])

    def hook(plot, element):
        plot.handles['plot'].min_border_left=4
        plot.handles['plot'].min_border_top=0
        plot.handles['plot'].min_border_right=0
        plot.handles['plot'].min_border_bottom=0


    bars = hv.Bars(
        data,
        kdims=['source_scene','metacluster_name'],
        vdims=['cell density']).opts(stacked=True,
                                     border=0,
                                   legend_position='right',
                                   height=1200,
                                   width=700,
                                   toolbar=None,
                                   tools=['hover'],
                                   cmap=cmap,
                                     yaxis=None,
                                   show_frame=False,
                                     invert_axes=True,
                                     hooks=[hook],
                                     align='center',
                                   ylabel='cell density (cells / mm\u00B2)')

    def hook(plot, element):
        plot.handles['plot'].min_border_left=0
        plot.handles['plot'].min_border_top=0
        plot.handles['plot'].min_border_right=0
        plot.handles['plot'].min_border_bottom=0


    df.grade = df.grade.astype('float').astype(int)#.astype('category')
    df.stage = df.stage.astype('str')

    df['source_scene'] = (df['source'] + '_' + df['scene'].astype(str)).astype('category')

    meta_df = df[['source_scene','grade','stage','clinical_subtype']].drop_duplicates()
    meta_df.source_scene.cat.set_categories(g.data2d.index,inplace=True)  # Set category sorting
    meta_df = meta_df.sort_values('source_scene')
    meta_df.clinical_subtype = meta_df.clinical_subtype.map({'TN':0,'HR+HER2-':1,'HR-HER2+':2,'HR+HER2+':3})
    meta_df.stage = meta_df.stage.map({'IIA':2, 'I':1, '0':0, 'IIB':3, 'IIIB':5, 'IIIA':4})


    meta_df['subtype label'] = 'clinical subtype'
    meta_df['grade label'] = 'grade'
    meta_df['stage label'] = 'stage'

    st = hv.HeatMap(meta_df,kdims=['subtype label','source_scene'],vdims='clinical_subtype').opts(yaxis=None,
    #                                                                           xaxis=None,
                                                                              xrotation=90,
                                                                                   xlabel='',
                                                                              toolbar=None,
                                                                              width=20,
                                                                              cmap=['#b3b3b3','#e5c494','#ffd92f','#a6d854'],
                                                                              height=1200,
                                                                              tools=['hover'],
                                                                                  hooks=[hook])

    grade = hv.HeatMap(meta_df,kdims=['grade label','source_scene'],vdims='grade').opts(yaxis=None,
    #                                                                           xaxis=None,
                                                                              xrotation=90,
                                                                                   xlabel='',
                                                                              toolbar=None,
                                                                              width=20,
                                                                              cmap=colorcet.CET_L19,
                                                                              height=1200,
                                                                              tools=['hover'],
                                                                                  hooks=[hook])

    stage = hv.HeatMap(meta_df,kdims=['stage label','source_scene'],vdims='stage').opts(yaxis=None,
    #                                                                           xaxis=None,
                                                                              xrotation=90,
                                                                                   xlabel='',
                                                                              toolbar=None,
                                                                              width=20,
                                                                              cmap=['white','#000c7c', '#5a0fa0', '#c80083', '#ff4d62', '#ffac28'],
                                                                              height=1200,
                                                                              tools=['hover'],
                                                                                  hooks=[hook])

    bar_fig = (dendros[0].opts(invert_axes=True,
                     invert_xaxis=True,
                     shared_axes=False,
                     height=1200,
                     width=150,
                     hooks=[hook]) + \
     grade + stage + st + 
     bars).cols(100)
    
    # box plot
    scale_factor = 0.325*1e-3 # mm/px
    density_dfs = []
    for source in tqdm(df.source.unique()):
        source_df = df[df.source==source]
        for scene in tqdm(source_df.scene.unique()):
            scene_df = source_df[(source_df.scene==scene)]
            hull = ConvexHull(scene_df[['centroid_x','centroid_y']]*scale_factor)
            density_df = (scene_df.groupby(['group','clinical_subtype']).size() / hull.volume).reset_index().rename(columns={0:'cell density'})
            density_df['source_scene'] = source + '_' + str(scene)
            density_dfs.append(density_df)

    group_density = pd.concat(density_dfs)
    group_density.group = group_density.group.map({0:'tumor',1:'immune',2:'stromal'})
    group_density.rename(columns={'group':'cell type', 'clinical_subtype': 'clinical subtype'},inplace=True)
    group_density.sort_values('source_scene',inplace=True)
    group_density.set_index('source_scene',inplace=True)

    group_density['normalized cell density'] = group_density.div(group_density.groupby(['source_scene'])['cell density'].sum().reset_index().set_index('source_scene'),axis=0)['cell density']

    box_fig = hv.BoxWhisker(group_density,
                      kdims=['cell type','clinical subtype'],
                      vdims=['normalized cell density']).opts(width=500,
                                                              height=400,
                                                   xrotation=90,
                                                   toolbar=None,
                                                              ylim=(0,1),
                                                   box_color='clinical subtype',
                                                   ylabel='cell type density / total cell density',
                                                   cmap=['#a6d854','#e5c494','#ffd92f','#b3b3b3']).sort()
    
    # Test immune density comparison
    cell_group = 'immune'

    group_density_norm = group_density[group_density['cell type']==cell_group].sort_values(by='source_scene')
    group_density_norm['cell density'] /= group_density.groupby(['source_scene'])['cell density'].sum().values

    # homogeneity of variances met
    print(f'comparing {cell_group} density across subtypes')
    stat, pval = bartlett(group_density_norm[(group_density_norm['clinical subtype']=='HR+HER2+') & (group_density_norm['cell type']==cell_group)]['cell density'],
                          group_density_norm[(group_density_norm['clinical subtype']=='HR+HER2-') & (group_density_norm['cell type']==cell_group)]['cell density'],
                          group_density_norm[(group_density_norm['clinical subtype']=='HR-HER2+') & (group_density_norm['cell type']==cell_group)]['cell density'],
                          group_density_norm[(group_density_norm['clinical subtype']=='TN') & (group_density_norm['cell type']==cell_group)]['cell density'])

    if pval < 0.05:
        print('homogeneity of variance NOT met, using Welch ANOVA and Games-Howell')
        sig_test = pg.pairwise_gameshowell(group_density_norm[group_density_norm['cell type']==cell_group],
                                           dv='cell density',
                                           between='clinical subtype')
    else:
        print('homogeneity of variance met, using ANOVA and Tukey')
        sig_test = pg.pairwise_tukey(group_density_norm[group_density_norm['cell type']==cell_group],
                                     dv='cell density',
                                     between='clinical subtype')
    print(sig_test)
    return (bar_fig + box_fig).opts(shared_axes=False).cols(9999)#, sig_test

def generate_fig6(df):

    # load data
    df = df[df.metacluster_name!='Segmentation error']
    df['source_scene'] = df.source + '_' + df.scene.astype(str)

    immune_clusters = [0,1,3,4,5,6]
    stroma_clusters = [6,8,9,10]
    endoth_clusters = [2,11]
    apopto_clusters = [7]
    cancer_clusters = [12,13,14,15,16,17,18,19,22,23,24,25,26]
    prolif_clusters = [20,21]

    map_dict = {
        **dict.fromkeys(cancer_clusters+apopto_clusters+prolif_clusters, 0),
        **dict.fromkeys(immune_clusters, 1),
        **dict.fromkeys(stroma_clusters+endoth_clusters, 2)
    }

    df['group'] = df.metacluster.map(map_dict)

    df.metacluster = df.metacluster.astype(int)

    # figure 6a
    n=10
    dist_thresh = round(65/0.325)

    total_counts = []
    for source in tqdm(df.source.unique()):
        source_df = df[df.source==source]
        for scene in tqdm(source_df.scene.unique()):
            scene_df = source_df[(source_df.scene==scene) & \
                                 (source_df.metacluster_name!='Segmentation error')].reset_index(drop=True)

            X = cudf.from_pandas(scene_df[['centroid_x','centroid_y']])

            model = NearestNeighbors(n_neighbors=n+1)

            model.fit(X)

            dist, idx = model.kneighbors(X)
            idx = idx[dist<dist_thresh] # drop neighbors greater than dist_thresh

            idx.drop(0,inplace=True) # drop self measurement
            idx = idx.replace(idx.index,scene_df.metacluster).to_pandas() # swap count for metacluster name

            idx += 1

            counts = idx.fillna(0).apply(np.bincount,minlength=28,axis=1)

            counts = pd.DataFrame(counts.tolist())
            counts['subtype'] = scene_df.clinical_subtype[0]
            counts['source_scene'] = source + '_' + str(scene)
            counts['cell_type'] = scene_df.group

            total_counts.append(counts)

    concat_counts = pd.concat(total_counts,ignore_index=True)
    metacluster_names = df[['metacluster_name', 
                            'metacluster']].drop_duplicates().sort_values(by='metacluster').metacluster_name.tolist()
    concat_counts.rename(columns={i+1: metacluster_names[i] for i in range(len(metacluster_names))},inplace=True)
    concat_counts.drop(0,axis='columns',inplace=True)

    colors = [
        '#008000', # B cell/T cell
        '#006400', # B cell
        '#FFFF00', # CD44+ endothelial
        '#808000', 
        '#7CFC00',
        '#556B2F',
        '#00BFFF',
        '#0000CD', # apoptotic
        '#87CEFA',
        '#B0E0E6',
        '#5F9EA0',
        '#CCCC00', # Vimentin+ endothelial
        '#FA8072',
        '#CD5C5C',
        '#FF0000',
        '#800000',
        '#FF8C00',
        '#FFC0CB',
        '#DC143C',
        '#9400D3',
        '#20B2AA',
        '#008080',
        '#DB7093',
        '#CD853F',
        '#FF69B4',
        '#FF1493',
        '#A0522D'
            ]

    name_mapper = df[['metacluster','metacluster_name']].drop_duplicates().sort_values(by='metacluster')
    cmap = dict(list(zip(name_mapper.metacluster_name.tolist(),colors)))

    total_nbr = concat_counts[concat_counts.cell_type==0].groupby('subtype')[[i for i in concat_counts.columns if i not in ['subtype','label','source_scene','cell_type']]].sum()

    tumor_nbr_bars = hv.Bars(
        pd.melt(
            total_nbr.div(total_nbr.sum(axis=1),axis='rows').reset_index(),
            id_vars=['subtype']
        ),
        kdims=['subtype','variable'],
        vdims=['value']
    ).opts(
        stacked=True,
        cmap=cmap,
        height=500,
        width=500,
        show_legend=False,
        tools=['hover'],
        toolbar=None,
        xlabel='clinical subtype',
        ylabel='proportion of tumor cell 10-nearest neighbors'
    )


    # figure 6bc
    dist_thresh = round(65/0.325)
    n=10
    total_counts = []
    group_counts = []
    for source in tqdm(df.source.unique()):
        source_df = df[df.source==source]
        for scene in tqdm(source_df.scene.unique()):
            scene_df = source_df[(source_df.scene==scene)&\
                                 (source_df.metacluster_name!='Segmentation error')].reset_index(drop=True)

            group_counts.append(scene_df.groupby(['source_scene', 'clinical_subtype','group']).size().unstack(fill_value=0).reset_index())

            X = cudf.from_pandas(scene_df[['centroid_x','centroid_y']])

            model = NearestNeighbors(n_neighbors=n+1)

            model.fit(X)

            dist, idx = model.kneighbors(X, return_distance=True)
            idx = idx[dist<dist_thresh]
            idx.drop(0,inplace=True)
            idx = idx.replace(idx.index,scene_df.group).to_pandas()
            idx += 1

            counts = idx.fillna(0).apply(np.bincount,minlength=4,axis=1)
            counts = pd.DataFrame(counts.tolist())
            counts['subtype'] = scene_df.clinical_subtype[0]
            counts['source_scene'] = source + '_' + str(scene)
            counts['cell_type'] = scene_df.group
            total_counts.append(counts)

    concat_counts = pd.concat(total_counts,ignore_index=True)

    concat_counts.rename(columns={1:'tumor',2:'immune',3:'stroma'},inplace=True)

    tumor_stroma_intx = concat_counts[
        concat_counts.cell_type==0
    ].groupby(
        [
            'subtype',
            'source_scene'
        ]
    )['stroma'].sum()

    stroma_stroma_intx = concat_counts[
        concat_counts.cell_type==2
    ].groupby(
        [
            'subtype',
            'source_scene'
        ]
    )['stroma'].sum()

    stromal_ratio = (df[df.group==2].groupby('source_scene').size()/df.groupby('source_scene').size()).reset_index().rename(columns={0:'stromal fraction'})
    stromal_ratio['clinical subtype'] = df.groupby(['source_scene','clinical_subtype']).size().reset_index().clinical_subtype

    # figure 6b
    stromal_frac_bars = hv.Bars(
        stromal_ratio,
        kdims=['source_scene'],
        vdims=['stromal fraction',
               'clinical subtype']
    ).opts(
        width=800,
        height=200,
        tools=['hover'],
        color='clinical subtype',
        xaxis='bare',
        toolbar=None,
        show_legend=False,
        line_width=0,
        cmap={'TN':'#b3b3b3',
              'HR+HER2-':'#e5c494',
              'HR+HER2+':'#a6d854',
              'HR-HER2+':'#ffd92f'},
        xrotation=45
    ).sort('stromal fraction') * \
    hv.HLine(0.25).opts(
        color='black',
        line_dash='dotted',
        line_width=1
    )

    stromal_mixing = (tumor_stroma_intx/stroma_stroma_intx).reset_index()
    stromal_mixing = stromal_mixing[stromal_mixing.source_scene.isin(stromal_ratio[stromal_ratio['stromal fraction']>0.25].source_scene.tolist())]

    # figure 6c inset
    stromal_mix_box = hv.BoxWhisker(
        stromal_mixing,
        kdims='subtype',
        vdims='stroma'
    ).opts(
        width=300,
        box_color='subtype',
        ylim=(0,3.25),
        cmap = {'TN':'#b3b3b3',
                'HR+HER2-':'#e5c494',
                'HR+HER2+':'#a6d854',
                'HR-HER2+':'#ffd92f'},
        ylabel='stromal mixing',
        xlabel='clinical subtype',
        toolbar=None
    )

    stat, pval = bartlett(stromal_mixing[(stromal_mixing['subtype']=='HR+HER2+')]['stroma'],
                          stromal_mixing[(stromal_mixing['subtype']=='HR+HER2-')]['stroma'],
                          stromal_mixing[(stromal_mixing['subtype']=='HR-HER2+')]['stroma'],
                          stromal_mixing[(stromal_mixing['subtype']=='TN')]['stroma'])

    if pval < 0.05:
        print('homogeneity of variance NOT met, using Welch ANOVA and Games-Howell')
        sig_test = pg.pairwise_gameshowell(stromal_mixing,
                                           dv='stroma',
                                           between='subtype')
    else:
        print('homogeneity of variance met, using ANOVA and Tukey')
        sig_test = pg.pairwise_tukey(stromal_mixing,
                                     dv='stroma',
                                     between='subtype')
    print(sig_test)

    # figure 6c
    stromal_mix_bars = hv.Bars(
        stromal_mixing,
            kdims='source_scene',
            vdims=['stroma','subtype']
    ).opts(
        color='subtype',
        cmap = {'TN':'#b3b3b3','HR+HER2-':'#e5c494','HR+HER2+':'#a6d854','HR-HER2+':'#ffd92f'},
        show_legend=False,
        xaxis='bare',
        tools=['hover'],
        toolbar=None,
        height=200,
        line_width=0,
        ylabel='stromal mixing',
        width=800
    ).sort('stroma')

    scale_factor = 0.325*1e-3 # mm/px
    density_dfs = []
    for source in tqdm(df.source.unique()):
        source_df = df[df.source==source]
        for scene in tqdm(source_df.scene.unique()):
            scene_df = source_df[(source_df.scene==scene)]
            hull = ConvexHull(scene_df[['centroid_x','centroid_y']]*scale_factor)
            density_df = (scene_df.groupby(['group','clinical_subtype']).size() / hull.volume).reset_index().rename(columns={0:'cell density'})
            density_df['source_scene'] = source + '_' + str(scene)
            density_dfs.append(density_df)

    group_densities = pd.concat(density_dfs)
    group_densities_norm = group_densities[group_densities['group']==2].sort_values(by='source_scene')
    group_densities_norm['cell density'] /= group_densities.groupby(['source_scene'])['cell density'].sum().values

    # figure 6d 
    stromal_scatter = hv.Scatter(
        stromal_mixing.merge(
            group_densities_norm[['source_scene','cell density']],
            on='source_scene'
        ),
        kdims='cell density',
        vdims=['stroma','subtype','source_scene']
    ).opts(
        color='subtype',
        cmap={'TN':'#b3b3b3',
              'HR+HER2-':'#e5c494',
              'HR+HER2+':'#a6d854',
              'HR-HER2+':'#ffd92f'},
        show_legend=False,
        toolbar=None,
        width=400,
        height=400,
        size=5,
        tools=['hover'],
        xlabel='stromal density / total cell density',
        ylabel='stromal mixing')

    fig = (
        tumor_nbr_bars + \
        stromal_frac_bars + \
        stromal_mix_bars + \
        stromal_mix_box + \
        stromal_scatter).opts(shared_axes=False).cols(1)
    return fig


def generate_fig7(df):
    
    df = df[df.metacluster_name!='Segmentation error']
    df['source_scene'] = df.source + '_' + df.scene.astype(str)
    df.metacluster = df.metacluster.astype(int)

    immune_clusters = [0,1,3,4,5,6]
    stroma_clusters = [6,8,9,10]
    endoth_clusters = [2,11]
    apopto_clusters = [7]
    cancer_clusters = [12,13,14,15,16,17,18,19,22,23,24,25,26]
    prolif_clusters = [20,21]

    map_dict = {
        **dict.fromkeys(cancer_clusters+apopto_clusters+prolif_clusters, 0),
        **dict.fromkeys(immune_clusters, 1),
        **dict.fromkeys(stroma_clusters+endoth_clusters, 2)
    }

    radius = 200 # 200 px --> 65 microns
    n_neighbors = 4
    query_clusters = cancer_clusters + prolif_clusters + apopto_clusters

    intx_dfs = []
    kc_dfs = []
    bc_dfs = []

    curr_dfs = []

    pbar1 = tqdm(df.source.drop_duplicates())
    for source in pbar1:
        pbar1.set_description(source)
        source_df = df[df.source==source]

        pbar2 = tqdm(source_df.scene.drop_duplicates(),leave=False)
        for scene in pbar2:
            pbar2.set_description(f'scene {scene}')

            curr_df = cudf.from_pandas(
                df[(df.source==source) & \
                   (df.scene==scene)].reset_index(drop=True)
            )


            hull = ConvexHull(curr_df[['centroid_x','centroid_y']].to_pandas())
            cx = np.mean(hull.points[hull.vertices,0])
            cy = np.mean(hull.points[hull.vertices,1])

            center = cp.array([cx,cy])

            dist = curr_df[['centroid_x','centroid_y']].values - center

            # punching 0.6mm virtual cores in center of real core
            mask = np.linalg.norm(dist, axis=1) < (300 / 0.325)
            curr_df = curr_df.iloc[mask].reset_index(drop=True)

            for query in [('cancer', cancer_clusters + prolif_clusters + apopto_clusters)]:#),
                          #('immune', immune_clusters),
                          #('stroma', stroma_clusters + endoth_clusters)]:

                query_df = curr_df[curr_df.metacluster.isin(query[1])].reset_index(drop=True)

                cc = get_intx(query_df, radius, n_neighbors)
                query_df[f'{query[0]}_centrality'] = cc

                curr_dfs.append(query_df)

    full_df = cudf.concat(curr_dfs).to_pandas()
    full_df.fillna(value=0,inplace=True)

    n_bins = 50
    n_cell = 100

    map_dict = {
        **dict.fromkeys(cancer_clusters+apopto_clusters+prolif_clusters, 0),
        **dict.fromkeys(immune_clusters, 1),
        **dict.fromkeys(stroma_clusters+endoth_clusters, 2)
    }

    full_df['group'] = full_df.metacluster.map(map_dict)

    # t = full_df.groupby(['source','scene']).size().reset_index()
    t = full_df.groupby(['source','scene','group']).count().unstack(fill_value=0).stack().reset_index()[['source','scene','group','cell']]#full_df.groupby(['source','scene','group']).size().reset_index()
    for source in t.source.unique():
        source_tmp = t[t.source==source]
        for scene in source_tmp.scene.unique():
            scene_tmp = source_tmp[source_tmp.scene==scene]
            if (scene_tmp.cell<30).any():
                full_df = full_df[~((full_df.source==source) & (full_df.scene==scene))]


    all_bins = []
    cell_types = [('cancer', cancer_clusters + apopto_clusters + prolif_clusters),]
                  #('immune', immune_clusters),
                  #('stroma', endoth_clusters + stroma_clusters)]

    for cell_type, cell_clusters in cell_types:

        group_df = full_df[full_df.metacluster.isin(cell_clusters)]
        group_df = group_df.groupby(['source',
                                     'scene'])[f'{cell_type}_centrality'].apply(np.histogram,
                                                                                bins=n_bins,
                                                                                range=(0,1),
                                                                                density=True).reset_index().sort_values(by=['source',
                                                                                                                            'scene'])
    #     group_df.drop(t[t[0]<n_cell].index,inplace=True)

        group_bins, bin_edges = zip(*group_df[f'{cell_type}_centrality'])
        columns = [f'{cell_type} {"%.2f" % round(bin_edges[0][i],2)}-{"%.2f" % round(bin_edges[0][i+1],2)}' for i in range(n_bins)]

        tmp_df = pd.DataFrame(group_bins,columns=columns)
    #     tmp_df /= tmp_df.values.max()
        all_bins.append(tmp_df)

    # Plot fig

    mpl.rcParams['figure.dpi'] = 300

    st = full_df[['source',
                  'scene',
                  'clinical_subtype']].drop_duplicates().sort_values(by=['source','scene']).clinical_subtype.reset_index(drop=True)
    lut = dict(zip(st.unique(), ['#ffd92f','#b3b3b3','#e5c494','#a6d854']))
    print(lut)
    st_colors = st.map(lut)

    full_df.loc[full_df.grade=='2.0','grade'] = '2'
    full_df.loc[full_df.grade=='-','grade'] = 'None'
    full_df.grade = full_df.grade.fillna(value='None')
    full_df.grade = full_df.grade.astype('str')
    grade = full_df[['source','scene','grade']].drop_duplicates().sort_values(by=['source','scene']).grade.reset_index(drop=True)
    lut = dict(zip(grade.unique(), "kmcw"))
    print(lut)
    grade_colors = grade.map(lut)

    full_df.stage = full_df.stage.fillna(value='None')
    stage = full_df[['source','scene','stage']].drop_duplicates().sort_values(by=['source','scene']).stage.reset_index(drop=True)
    lut = dict(zip(stage.unique(), ['r','b','g','orange','brown']))#"rbgyw"))
    print(lut)
    stage_colors = stage.map(lut)

    label_colors = pd.DataFrame()
    # label_colors['grade'] = grade_colors
    # label_colors['stage'] = stage_colors
    label_colors['clinical subtype'] = st_colors

    # columns = [ct[0] + ' ' + col for ct in cell_types for col in columns]
    hm_df = pd.concat(all_bins,axis=1)#pd.DataFrame(bins,columns=columns).fillna(0)
    hm_df = hm_df.div(n_bins,axis=0)
    # hm_df.drop(pd.Index(drop_scene),inplace=True)

    g = sns.clustermap(hm_df,
                       cmap='magma',
                       cbar_pos=(1, 0.5, 0.05, 0.18),
                       col_cluster=False,
                       figsize=(8,8),
                       method='average',
                       metric='jensenshannon',
                       row_colors=label_colors)

    # g.ax_heatmap.yaxis.set_ticks([])
    g.ax_heatmap.xaxis.set_tick_params(labelsize=5)
    g.ax_heatmap.yaxis.set_tick_params(labelsize=3)

    g.ax_row_colors.xaxis.set_tick_params(labelsize=5)
    g.ax_cbar.yaxis.set_tick_params(labelsize=5)

            
def get_intx(curr_df, radius, n_neighbors):

    if len(curr_df) > n_neighbors:
        knn = cuml.neighbors.NearestNeighbors(n_neighbors=n_neighbors+1)
        knn.fit(curr_df[['centroid_x','centroid_y']])

        distances, indices = knn.kneighbors(curr_df[['centroid_x','centroid_y']])

        check = indices.mask(distances>radius).fillna(-1).to_pandas()

        edges = pd.DataFrame(
            {'source': np.repeat(check[0].to_numpy(),n_neighbors),
             'target': check[list(range(1,n_neighbors+1))].to_numpy().flatten(),
             'weight': distances[list(range(1,n_neighbors+1))].to_pandas().to_numpy().flatten()})
        edges = edges[edges.target!=-1]

        if len(edges) > 0:
            G = nx.from_pandas_edgelist(edges)
            cc = cudf.Series(nx.closeness_centrality(G,distance='weight'))
            cc /= cc.max()
        else:
            cc = cudf.Series(cp.zeros(len(curr_df)))

    else:
        cc = cudf.Series(cp.zeros(len(curr_df)))

    return cc