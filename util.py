from scipy.cluster.hierarchy import linkage, dendrogram
from collections import defaultdict
import numpy as np
import holoviews as hv

def nested_dict():
    """
    A nested dictionary for hierarchical storage of thresholds.
    """
    return defaultdict(nested_dict)

### adapted from https://anaconda.org/philippjfr/dendrogram/notebook

def compute_linkage(dataset, dim, vdim):
    arrays, labels = [], []
    for k, v in dataset.groupby(dim, container_type=list, group_type=hv.Dataset):
        labels.append(k)
        arrays.append(v.dimension_values(vdim))
    X = np.vstack(arrays)
    X = np.ma.array(X, mask=np.logical_not(np.isfinite(X)))
    Z = linkage(X,method='ward')
    ddata = dendrogram(Z, labels=labels, no_plot=True)
    ddata['mh'] = np.max(Z[:, 2])
    return ddata, Z

### adapted from https://anaconda.org/philippjfr/dendrogram/notebook

def get_dendrogram(dataset, dim, vdim, width=600, height=800, dendro_side='right'):
    if not isinstance(dim, list): dim = [dim]
    kdims = dataset.kdims
    dataset = hv.Dataset(dataset)
    sort_dims, dendros = [], []
    for i, d in enumerate(dim):
        ddata, _ = compute_linkage(dataset, d, vdim)
        mh = ddata['mh']
        order = [ddata['ivl'].index(v) for v in dataset.dimension_values(d)][::-1]
        sort_dim = 'sort%s' % i
        dataset = dataset.add_dimension(sort_dim, 0, order)
        sort_dims.append(sort_dim)
        ivw = len(ddata['ivl']) * 10
        dvw = mh + mh * 0.05
        extents = (0, 0, ivw, dvw)
        dendro = hv.Path(list(zip(ddata['icoord'], ddata['dcoord'])),
                         group='Dendrogram', extents=extents).opts(line_color='black',
                                                                   xaxis=None,
                                                                   yaxis=None,
                                                                   show_grid=False,
                                                                   show_title=False,
                                                                   show_frame=False,
                                                                   border=0,
                                                                   tools=['hover'])
        dendros.append(dendro)
    
        
    opts = [hv.opts.Path(width=80,height=height,invert_yaxis=False), hv.opts.Path(height=80,width=width,invert_xaxis=True)]
    if len(sort_dims) == 1: sort_dims = kdims[:1] + sort_dims
    vdims = [dataset.get_dimension(vdim)]+[vd for vd in dataset.vdims if vd != vdim]
    dataset = hv.HeatMap(dataset.sort(sort_dims).reindex(kdims), vdims=vdims).opts(width=width,
                                                                                   height=height,
                                                                                   border=0,
                                                                                   yaxis=None,
                                                                                   xrotation=90,
                                                                                   tools=['hover'],
                                                                                   colorbar=True,
                                                                                   colorbar_position='bottom',
                                                                                   cmap='Spectral_r',
                                                                                   symmetric=True)
    
    for dendro, opt in zip(dendros, opts):            
        dataset = dataset << dendro.opts(opt)
    return dataset

def get_dendrogram_short(dataset, dim, vdim, width=600, height=800, dendro_side='right'):
    if not isinstance(dim, list): dim = [dim]
    kdims = dataset.kdims
    dataset = hv.Dataset(dataset)
    sort_dims, dendros = [], []
    for i, d in enumerate(dim):
        ddata, _ = compute_linkage(dataset, d, vdim)
        mh = ddata['mh']
        order = [ddata['ivl'].index(v) for v in dataset.dimension_values(d)][::-1]
        sort_dim = 'sort%s' % i
        dataset = dataset.add_dimension(sort_dim, 0, order)
        sort_dims.append(sort_dim)
        ivw = len(ddata['ivl']) * 10
        dvw = mh + mh * 0.05
        extents = (0, 0, ivw, dvw)
        dendro = hv.Path(list(zip(ddata['icoord'], ddata['dcoord'])),
                         group='Dendrogram', extents=extents).opts(line_color='black',
                                                                   xaxis=None,
                                                                   yaxis=None,
                                                                   show_grid=False,
                                                                   show_title=False,
                                                                   show_frame=False,
                                                                   border=0,
                                                                   tools=['hover'])
        dendros.append(dendro)
    return dendros