import os
import pickle
import argparse
import warnings
import numpy as np
import pandas as pd
import holoviews as hv
from natsort import natsorted
from markers import me_markers
from data import nested_dict, process_data
from clustering import get_ssc_thresh

warnings.simplefilter('ignore')
hv.extension('matplotlib','bokeh')

np.random.seed(seed=42)

def run(source, pos_idx, save_dir):
    
    print(source)
    
    markers = list(me_markers.keys())[int(pos_idx)]
    pos = markers
    source_threshs_dir = f'{save_dir}/img/threshs/{source}'
    os.makedirs(source_threshs_dir, exist_ok=True)
    
    df = pd.read_csv(f'{save_dir}/master_TMA_raw_mean_intensities.csv')
    source_df = df[df.source==source]

    thresh_dict = nested_dict()

    for neg in me_markers[pos]:
        
        print(neg)

        figs = []

        for scene in natsorted(set(source_df.scene)):
            
            print(scene)

            tmp_df = source_df[source_df.scene==scene][[pos,neg]]
            tmp_df = process_data(tmp_df,pos,neg)
            thresh, clusters = get_ssc_thresh(tmp_df)

            thresh_dict[source][scene][pos][neg] = [thresh]

            xlim = (source_df[pos].quantile(0.001),
                    source_df[pos].quantile(0.999))

            ylim = (source_df[neg].quantile(0.001),
                    source_df[neg].quantile(0.999))

            scatters = hv.Overlay([hv.Scatter(i).opts(xlabel=pos,
                                                      ylabel=neg,
                                                      xlim=xlim,
                                                      ylim=ylim,
                                                      alpha=0.1) for i in clusters])

            thresh_line = hv.VLine(thresh).opts(color='black')

            fig = hv.Overlay([scatters, thresh_line],label=f'scene {str(scene)}')

            figs.append(fig)

        layout = hv.Layout(figs).opts(title=source,sublabel_format='').cols(4)
        hv.save(layout,f'{source_threshs_dir}/{source}_{pos}_{neg}.png')
    
    thresh_dir = f'{save_dir}/thresh_dicts/{source}'
    os.makedirs(thresh_dir,exist_ok=True)
    
    pickle.dump(thresh_dict, open(f'{thresh_dir}/{source}_{pos}_thresh_dict.pkl','wb'))

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='TMA normalization')
    parser.add_argument('--source', type=str, default=None, metavar='S', help='which TMA to process')
    parser.add_argument('--pos_idx', type=str, default='~/', metavar='S', help='which marker to normalize')
    parser.add_argument('--save_dir', type=str, default='~/', metavar='S', help='where to save')
    args = parser.parse_args()
    
    run(args.source, args.pos_idx, args.save_dir)