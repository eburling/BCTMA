import numpy as np
import pandas as pd
import holoviews as hv
from scipy.cluster.hierarchy import cut_tree
import seaborn as sns
from holoviews import dim
from holoviews.operation.datashader import datashade
from scipy.stats import zscore
from util import compute_linkage, get_dendrogram
from sklearn.preprocessing import minmax_scale, scale
from natsort import natsorted
from colorcet import glasbey_hv
import datashader as ds


def generate_fig2d(df):
    hv.extension('bokeh')
    
    df = df[df.metacluster_name != 'Segmentation error']
    
    community = '40_neighbor'
    stain_exc = ['PD1','CD8']
    feature_cols = [i for i in df.columns if 'Enorm' in i and
                    not any(j in i for j in stain_exc)]
    hmap_df = df[feature_cols+[community]].copy()
    feature_map = {i:i.split('_')[2] for i in feature_cols}
    feature_cols = list(feature_map.values())
    hmap_df.rename(columns=feature_map,inplace=True)
    hmap_df.loc[:,feature_cols] = hmap_df.loc[:,feature_cols].clip(upper=hmap_df.loc[:,feature_cols].quantile(q=.99),axis=1)

    g = sns.clustermap(hmap_df.groupby(community).mean().drop(-1.0,errors='ignore'),
                       z_score=1,
                       cmap='Spectral_r',
                       method='ward',
                       center=0,
                       figsize=(12,20))
    height = 800

    melted = pd.melt(g.data2d.reset_index(),
                     id_vars=community,
                     var_name='marker',
                     value_name='expression')

    hm = hv.HeatMap(melted,kdims=['marker',community]).opts(width=700,
                                                              height=height,
                                                              border=0,
                                                              tools=['hover'],
                                                              colorbar=True,
                                                              colorbar_position='bottom',
                                                              cmap='Spectral_r',
                                                              symmetric=True)

    hm_fig = get_dendrogram(hm, [community,'marker'], 'expression')

    # Define subtype/cluster cell counts bars
    community_sort = melted[community].drop_duplicates()
    counts = df.groupby([community,'clinical_subtype']).size().drop(-1.0,errors='ignore')   # Count cells in each community
    counts = counts.unstack(fill_value=0).stack().to_frame().reset_index()                  # Make sure to include zeros
    counts[community] = counts[community].astype('category')                                # Make categorical for sorting
    counts[community].cat.set_categories(community_sort,inplace=True)                       # Set category sorting

    subtype_sort = reversed(['TN','HR+HER2-','HR-HER2+','HR+HER2+'])
    counts.clinical_subtype = counts.clinical_subtype.astype('category')      # Make categorical for sorting
    counts.clinical_subtype.cat.set_categories(subtype_sort,inplace=True)  # Set category sorting

    counts = counts.sort_values([community,'clinical_subtype']).rename(columns={0:'cell count'}).iloc[::-1] # Sort and rename
    counts['cell count'] = counts['cell count'].div(1e4)                 # Rescale for axis ticks                 

    def hook(plot, element):
        plot.handles['plot'].min_border_right=5

    bars = hv.Bars(counts,[community,'clinical_subtype'],'cell count').opts(invert_axes=True,
                                                                              invert_xaxis=True,
                                                                              invert_yaxis=True,
                                                                              show_frame=False,
                                                                              width=170,
                                                                              height=height,
                                                                              yaxis=None,
                                                                              ylabel=f'cells in cluster (×10\u2074)',
                                                                              border=0,
                                                                              show_legend=False,
                                                                              tools=['hover'],
                                                                              stacked=True,
                                                                              cmap='Set2_r',
                                                                              hooks=[hook])

    # Define subtype/cluster proportion scatter plot
    dot = df.groupby(['clinical_subtype',community]).size().unstack(fill_value=0).stack().reset_index().rename(columns={0:'counts'})
    dot = dot[dot[community]!=-1]
    dot[community] = dot[community].astype('category')
    dot[community].cat.set_categories(community_sort[::-1],inplace=True)

    sorted_dot = []
    for cs in ['HR+HER2+', 'HR-HER2+', 'HR+HER2-', 'TN']:
        total = dot[dot.clinical_subtype==cs].counts.sum()
        dot.loc[dot.clinical_subtype==cs,'counts'] = (dot.loc[dot.clinical_subtype==cs]['counts'] / total) * 100
        dot.loc[dot.clinical_subtype==cs] = dot.loc[dot.clinical_subtype==cs].iloc[community_sort[::-1]]
        sorted_dot.append(dot.loc[dot.clinical_subtype==cs].sort_values(community))

    dot = pd.concat(sorted_dot).rename(columns={'counts':'% of subtype'}).iloc[::-1]
    dot[community] = dot[community].astype(str)

    def hook(plot, element):
        plot.handles['plot'].min_border_right=5
        plot.handles['plot'].min_border_right=5

    scat = hv.Scatter(dot).opts(height=height,
                                size=dim('% of subtype'),
                                tools=['hover'],
                                border=0,
                                show_frame=False,
                                alpha=0.5,
                                yaxis=None,
                                xlabel='clinical subtype',
                                xrotation=90,
                                width=120,
                                color=dim('clinical_subtype'),
                                cmap='Set2_r',
                                show_legend=False,
                                hooks=[hook])

    # Define metacommunities based on tree cut
    _, Z = compute_linkage(hm,community,'expression')
    n_clusters = 25
    cut = cut_tree(Z,n_clusters=n_clusters)[::-1]

    # Map communities to metacommunities
    meta = [('metacluster',community_sort[::-1].to_list()[i],cut[i][0]) for i in range(cut.shape[0])]
    df['metacluster'] = df[community].map({i[1]:i[-1] for i in meta})

    # Rename metaclusters in order of size
    meta_counts = df.groupby('metacluster').size().sort_values()[::-1].reset_index()
    meta_sorted = dict(zip(meta_counts['metacluster'],meta_counts.index))
    meta = [(i[0],i[1],meta_sorted[i[2]]) for i in meta]

    df['metacluster'] = df['metacluster'].map(meta_sorted)
    cmap = hv.Cycle('Category20')._values_param_value
    meta = hv.HeatMap(meta,
                      ['cluster type', community],
                      'metacluster').opts(cmap=cmap[:n_clusters],
                                            height=height,
                                            width=16,
                                            xlabel=' ',
                                            xrotation=90, 
                                            tools=['hover'],
                                            yaxis=None,
                                            border=0)


    # Dropped in to override metacluster labels from tree cut
    cell_types = {
        34: 0,  #'B cell/T cell',
        26: 1,  #'B cell',
        11: 2,  #'CD44+ endothelial',
        3:  3,  #'T cell/macrophage',
        2:  4,  #'T cell',
        38: 5,  #'macrophage',
        21: 4,  #'T cell',
        48: 6,  #'PDPN+/aSMA+ stromal',
        29: 7,  #'apoptotic',
        33: 7,  #'apoptotic',
        28: 7,  #'apoptotic',
        43: 6,  #'PDPN+/aSMA+ stromal',
        1:  8,  #'ColI+ stromal',
        24: 9,  #'Lamin+/Vimentin+ stromal',
        0:  9,  #'Lamin+/Vimentin+ stromal',
        27: 10, #'ColI+/PDPN+ stromal',
        5:  11, #'Vimentin+ endothelial',
        18: 12, #'Ecad+/CK low',
        14: 12, #'Ecad+/CK+',
        23: 12, #'Ecad+/CK+',
        8:  13, #'CK5+',
        42: 13, #'CK5+',
        40: 14, #'CK5+/CK14+',
        37: 14, #'CK5+/CK14+',
        41: 15, #'myoepithelial',
        13: 16, #'CK+/H3K27me3+',
        12: 16, #'CK+/H3K27me3+',
        19: 16, #'CK+/H3K27me3+',
        35: 17, #'epithelial low',
        25: 17, #'epithelial low',
        22: 17, #'epithelial low',
        4:  17, #'epithelial low',
        39: 17, #'epithelial low',
        16: 18, #'CK14+',
        36: 19, #'Ecad+/CK+',
        7:  19, #'Ecad+/CK+',
        20: 20, #'Ki67+/PCNA+ proliferating',
        44: 21, #'H3K4+',
        47: 20, #'Ki67+/pHH3+ proliferating',
        31: 22, #'HER2+/CK8+',
        15: 22, #'HER2+/CK8+',
        10: 23, #'ER+',
        6:  24, #'HER2 low',
        45: 24, #'HER2 low',
        32: 25, #'HER2+/CK+',
        9:  25, #'HER2+/CK+',
        46: 25, #'HER2+/CK+',
        30: 26, #'ER+/PgR+',
        17: 26, #'ER+/PgR+'
    }

    meta.data.metacluster = meta.data['40_neighbor'].replace(cell_types).astype(int)

    cmap = [
        '#008000', # B cell/T cell
        '#006400', # B cell
        '#FFFF00', # CD44+ endothelial
        '#808000', 
        '#7CFC00',
        '#556B2F',
        '#00BFFF',
        '#0000CD', # apoptotic
        '#87CEFA',
        '#B0E0E6',
        '#5F9EA0',
        '#CCCC00', # Vimentin+ endothelial
        '#FA8072',
        '#CD5C5C',
        '#FF0000',
        '#800000',
        '#FF8C00',
        '#FFC0CB',
        '#DC143C',
        '#9400D3',
        '#20B2AA',
        '#008080',
        '#DB7093',
        '#CD853F',
        '#FF69B4',
        '#FF1493',
        '#A0522D'
            ]

    meta = hv.HeatMap(meta,
                      ['cluster type', community],
                      'metacluster').opts(cmap=cmap,
                                            height=height,
                                            width=16,
                                            xlabel=' ',
                                            xrotation=90, 
                                            tools=['hover'],
                                            yaxis=None,
                                            border=0)

    # Making single-cell heatmap
    hmap_df['metacluster'] = df['40_neighbor'].replace(cell_types) #df['metacluster']
    hmap_df[community] = hmap_df[community].astype('category')
    hmap_df[community].cat.set_categories(community_sort,inplace=True)

    xx = hmap_df.sort_values(community)[feature_cols+[community,'metacluster']]
    from matplotlib.colors import rgb2hex

    spectral = [rgb2hex(rgb) for rgb in hv.Palette('Spectral')._values_param_value]
    datashade.cmap=spectral

    sorted_markers = hm_fig.data['main'].data.marker.drop_duplicates().to_list()
    xx_marker_cols = xx.columns[:-1]
    ds_heatmap = np.clip(zscore(xx[xx_marker_cols][sorted_markers][::-1].to_numpy()),-2,2)

    n_features = len(sorted_markers)
    first_feat = (-0.5) + (0.5 / n_features)
    last_feat  = (0.5) - (0.5 / n_features)
    xticks = list(zip(np.linspace(first_feat,last_feat,n_features),sorted_markers))

    sc_hm = datashade(hv.Image(ds_heatmap),normalization='linear').opts(toolbar='above',
                                                 yaxis=None,
                                                 xticks=xticks,
                                                 xlabel='',
                                                 width=600,
                                                 height=height,
                                                 xrotation=90,
                                                 border=2)

    # Making binary single-cell heatmap
    hmap_index = xx[xx_marker_cols][sorted_markers].index
    xticks = list(zip(np.linspace(first_feat,last_feat,n_features),sorted_markers))

    ds_heatmap = hmap_df.loc[hmap_index][sorted_markers][::-1].to_numpy()
    ds_heatmap[ds_heatmap<=1]=-1
    ds_heatmap[ds_heatmap>1]=1
    datashade.cmap=[spectral[0],spectral[-1]]
    sc_binary_hm = datashade(hv.Image(ds_heatmap),normalization='log').opts(toolbar='above',
                                                        yaxis=None,
                                                        xticks=xticks,
                                                        xlabel='',
                                                        width=600,
                                                        height=height,
                                                        xrotation=90,
                                                        border=2)

    # Make single-cell metabars
    xx_counts = xx.groupby('metacluster',sort=False).size().reset_index()[::-1]
    xx_counts = xx_counts.rename(columns={0:'count','metacluster':'cluster'})
    xx_counts['metacluster'] = 'metacluster'
    xx_counts.cluster = xx_counts.cluster.astype('int64')

    def hook(plot, element):
        plot.handles['plot'].min_border_left=10
        plot.handles['plot'].min_border_right=0

    meta_bars = hv.Bars(xx_counts,['metacluster','cluster']).opts(color=dim('cluster'),
                                                                  cmap=cmap,
                                                                  stacked=True,
                                                                  bar_width=1,
                                                                  height=height,
                                                                  tools=['hover'],
                                                                  xlabel='',
                                                                  xrotation=90,
                                                                  yaxis=None,
                                                                  width=25,
                                                                  line_alpha=0,
                                                                  invert_yaxis=True,
                                                                  border=2,
                                                                  show_legend=False,
                                                                  hooks=[hook])

    cell_strings = {
        34: 'B cell/T cell',
        26: 'B cell',
        11: 'CD44+ endothelial',
        3:  'T cell/macrophage',
        2:  'T cell',
        38: 'macrophage',
        21: 'T cell',
        48: 'PDPN+/aSMA+ stromal',
        29: 'apoptotic',
        33: 'apoptotic',
        28: 'apoptotic',
        43: 'PDPN+/aSMA+ stromal',
        1:  'ColI+ stromal',
        24: 'Lamin+/Vimentin+ stromal',
        0:  'Lamin+/Vimentin+ stromal',
        27: 'ColI+/PDPN+ stromal',
        5:  'Vimentin+ endothelial',
        18: 'Ecad+/CK low',
        14: 'Ecad+/CK+',
        23: 'Ecad+/CK+',
        8:  'CK5+',
        42: 'CK5+',
        40: 'CK5+/CK14+',
        37: 'CK5+/CK14+',
        41: 'myoepithelial',
        13: 'CK+/H3K27me3+',
        12: 'CK+/H3K27me3+',
        19: 'CK+/H3K27me3+',
        35: 'epithelial low',
        25: 'epithelial low',
        22: 'epithelial low',
        4:  'epithelial low',
        39: 'epithelial low',
        16: 'CK14+',
        36: 'Ecad+/CK+',
        7:  'Ecad+/CK+',
        20: 'proliferating',
        44: 'H3K4me3+',
        47: 'proliferating',
        31: 'HER2+/CK8+',
        15: 'HER2+/CK8+',
        10: 'ER+',
        6:  'HER2 low',
        45: 'HER2 low',
        32: 'HER2+/CK+',
        9:  'HER2+/CK+',
        46: 'HER2+/CK+',
        30: 'ER+/PgR+',
        17: 'ER+/PgR+'
    }

    legend_df = meta.data.drop_duplicates(subset='metacluster').replace({'40_neighbor':cell_strings})

    def hook(plot, element):
        plot.handles['plot'].min_border_top=0
        plot.handles['plot'].min_border_right=20

    legend = hv.HeatMap(legend_df).opts(cmap=cmap,
                                        width=204,
                                        height=400,
                                        ylabel='metacluster',
                                        xaxis=None,
                                        toolbar=None,
                                        hooks=[hook])

    key_df = pd.DataFrame({'% of clinical subtype':100*np.linspace(0.05,0.20,num=4)})
    key_df['foo'] = 'foo'
    def hook(plot, element):
        plot.handles['plot'].min_border_left=10
        plot.handles['plot'].min_border_right=10

    key_scat = hv.Scatter(key_df).opts(size=dim('% of clinical subtype'),
                            color='black',
                            padding=0.1,
                            tools=['hover'],
                           yaxis=None,
                           height=75,
                            width=125,
                           show_frame=False,
                           toolbar=None,
                           xlim=(2,23),  
                           xticks=[5,10,15,20],
                                       border=0,
                                      hooks=[hook])

    # Display final figure
    fig = key_scat + legend + bars + scat + meta + hm_fig.redim.range(expression=(-2, 2)) + meta_bars + sc_binary_hm
    return fig.opts(shared_axes=False).cols(100)

def generate_fig3acd(merged_df):

    merged_df.rename(columns={'H3K27':'H3K27me3'},inplace=True)
    
    # figure 3a
    merged_df = merged_df[merged_df.clinical_subtype!='None']
    merged_df.clinical_subtype = merged_df.clinical_subtype.map({'ER':'HR+HER2-','HER2':'HR-HER2+','HER2/ER':'HR+HER2+','TN':'TN'})
    merged_df.clinical_type = merged_df.clinical_type.map({'HR+HER2-':'HR+HER2-','HR-HER2+':'HR-HER2+','HR+HER2+':'HR+HER2+','TripleNeg':'TN'})
    ohsu_st_counts = merged_df[(merged_df.source!='basel')&(merged_df.source!='zurich')].groupby('clinical_subtype').size().reset_index().rename(columns={0:'cellular ratio','clinical_subtype':'clinical subtype'})
    ohsu_st_counts['cellular ratio'] = ohsu_st_counts['cellular ratio'] / ohsu_st_counts['cellular ratio'].sum()
    ohsu_st_counts['cohort'] = 'OHSU'

    basel_st_counts = merged_df[merged_df.source=='basel'].groupby('clinical_type').size().reset_index().rename(columns={0:'cellular ratio', 'clinical_type':'clinical subtype'})
    basel_st_counts['cellular ratio'] = basel_st_counts['cellular ratio'] / basel_st_counts['cellular ratio'].sum()
    basel_st_counts['cohort'] = 'Basel'

    comp_bars = hv.Bars(pd.concat([ohsu_st_counts,basel_st_counts]),
                        kdims=['clinical subtype','cohort'],
                        vdims='cellular ratio').opts(width=500,
                                                     toolbar=None,
                                                     padding=0.1,
                                                     cmap={'Basel':'#ecf1f9',
                                                           'OHSU':'#fff1ce'})

    not_boden = (merged_df.source!='zurich')&(merged_df.source!='basel')

    shared_features = [
        'CD20',
        'CD31',
        'CD44',
        'CD45',
        'CD68',
        'CK14',
        'CK19',
        'CK5',
        'CK7',
        'CK8',
        'ER',
        'Ecad',
        'H3K27me3',
        'HER2',
        'Ki67',
        'PgR',
        'Vim',
        'aSMA',
        'cPARP',
        'p-HH3',
        'p-S6'
    ]

    ohsu_cm = sns.clustermap(merged_df.loc[not_boden,shared_features+['20_neighbor']].groupby('20_neighbor').mean().drop(index=-1.0),
                       z_score=1,
                       cmap='Spectral_r',
                       method='ward',
                       center=0,
                       figsize=(12,12))

    height = 1000

    melted = pd.melt(ohsu_cm.data2d.reset_index(),
                     id_vars='20_neighbor',
                     var_name='marker',
                     value_name='expression')

    ohsu_hm = hv.HeatMap(melted,kdims=['marker','20_neighbor']).opts(width=700,
                                                              height=height,
                                                              border=0,
                                                              tools=['hover'],
                                                              colorbar=True,
                                                              colorbar_position='bottom',
                                                              cmap='Spectral_r',
                                                              symmetric=True)

    ohsu_hm_fig = get_dendrogram(ohsu_hm, ['20_neighbor','marker'], 'expression').redim.range(expression=(-2, 2))

    boden_g = sns.clustermap(merged_df.loc[merged_df.source=='basel',shared_features+['20_neighbor']].groupby('20_neighbor').mean().drop(index=-1),
                       z_score=1,
                       cmap='Spectral_r',
                       method='ward',
                       center=0,
                       figsize=(12,20))

    height = 1000

    melted = pd.melt(boden_g.data2d.reset_index(),
                     id_vars='20_neighbor',
                     var_name='marker',
                     value_name='expression')

    boden_hm = hv.HeatMap(melted,kdims=['marker','20_neighbor']).opts(width=700,
                                                              height=height,
                                                              border=0,
                                                              tools=['hover'],
                                                              colorbar=True,
                                                              colorbar_position='bottom',
                                                              cmap='Spectral_r',
                                                              symmetric=True)

    boden_hm_fig = get_dendrogram(boden_hm, ['20_neighbor','marker'], 'expression').redim.range(expression=(-2, 2))
    l = boden_hm_fig.right.opts(invert_axes=True,invert_xaxis=True,invert_yaxis=False)
    boden_hm_fig.data['right'] = hv.Empty()

    ohsu_clust = merged_df.loc[not_boden,shared_features+['20_neighbor']].groupby('20_neighbor').mean().drop(index=-1).apply(scale)
    ohsu_clust_map = ohsu_hm_fig.data['main'].data['20_neighbor'].drop_duplicates().reset_index(drop=True).to_dict()
    ohsu_clust_map = {int(v): k for k, v in ohsu_clust_map.items()}

    boden_clust = merged_df.loc[merged_df.source=='basel',shared_features+['20_neighbor']].groupby('20_neighbor').mean().drop(index=-1).apply(scale)
    boden_clust_map = boden_hm_fig.data['main'].data['20_neighbor'].drop_duplicates().reset_index(drop=True).to_dict()
    boden_clust_map = {int(v): k for k, v in boden_clust_map.items()}

    clust_match = {}
    for idx, row in ohsu_clust.iterrows():
        corr = boden_clust.corrwith(row,axis=1,method='pearson')
        clust_match[ohsu_clust_map[int(idx)]] =  [boden_clust_map[int(corr.idxmax())], corr.max()]

    boden_clust = boden_clust.reindex(index=natsorted(boden_clust.index))
    ohsu_clust = ohsu_clust.reindex(index=natsorted(ohsu_clust.index))

    corr_dfs = []
    corr_series = {}
    for idx, row in ohsu_clust.iterrows():
        corr = boden_clust.corrwith(row,axis=1,method='pearson')
        corr.name = "Pearson's r"
        corr_df = corr.reset_index()
        corr_series[int(row.name)] = corr
        corr_df['OHSU cluster'] = int(row.name)
        corr_df.rename(columns={'20_neighbor':'Basel cluster',0:"Pearson's r"},inplace=True)
        corr_dfs.append(corr_df)

    g = sns.clustermap(pd.DataFrame(corr_series),
                       cmap='coolwarm',
                       method='ward',
                       center=0,
                       figsize=(18,18))

    corr_df = pd.DataFrame(corr_series)

    ohsu2boden = pd.DataFrame(corr_df.max(axis=0)).rename(columns={0:'max corr.'})
    ohsu2boden['comparison'] = 'OHSU-to-Basel'

    boden2ohsu = pd.DataFrame(corr_df.max(axis=1)).rename(columns={0:'max corr.'})
    boden2ohsu['comparison'] = 'Basel-to-OHSU'

    corr_box = hv.BoxWhisker(pd.concat((ohsu2boden,boden2ohsu)),['comparison']).opts(padding=0.1,ylim=(0,1),width=300,toolbar=None)

    height = 1000

    melted = pd.melt(g.data2d.reset_index(),
                     id_vars='20_neighbor',
                     var_name='marker',
                     value_name='corr')

    hm = hv.HeatMap(melted,kdims=['marker','20_neighbor']).opts(width=1000,
                                                              height=1000,
    #                                                           border=0,
                                                              tools=['hover'],
                                                              colorbar=True,
                                                              colorbar_position='bottom',
                                                              cmap='coolwarm',
                                                              symmetric=True)

    hm_fig = get_dendrogram(hm, ['20_neighbor','marker'], 'corr', width=1000,height=1000)
    hm_fig.data['main']=hm_fig.data['main'].opts(cmap='coolwarm',xaxis=None)#,width=1000,height=1000)

    ohsu_corr_order = g.dendrogram_col.dendrogram['ivl']
    boden_corr_order = g.dendrogram_row.dendrogram['ivl']

    ohsu_hm.data['20_neighbor'] = ohsu_hm.data['20_neighbor'].astype(int).astype(str).astype('category')
    ohsu_hm.data['20_neighbor'].cat.set_categories(ohsu_corr_order,inplace=True)

    ohsu_hm.data['marker'] = ohsu_hm.data['marker'].astype('category')
    ohsu_hm.data['marker'].cat.set_categories(ohsu_hm.data['marker'].drop_duplicates().tolist(),inplace=True)
    ohsu_hm.data = ohsu_hm.data.sort_values(by=['marker','20_neighbor'])

    ohsu_hm = hv.HeatMap(ohsu_hm.data.reset_index(drop=True),kdims=['marker','20_neighbor'],vdims='expression').opts(width=500,
                                                              height=height,
                                                              border=0,
                                                              tools=['hover'],
                                                              colorbar=True,
                                                              colorbar_position='bottom',
                                                              cmap='Spectral_r',
                                                              symmetric=True).redim.range(expression=(-2, 2))

    for dendro in [hv.Empty(),ohsu_hm_fig.data['top'].opts(width=500,invert_xaxis=False)]:
        ohsu_hm = ohsu_hm<<dendro

    def hook(plot, element):
        plot.handles['plot'].min_border_top=0
        plot.handles['plot'].min_border_bottom=10

    final_ohsu_hm = ohsu_hm.opts(hv.opts.HeatMap(xrotation=90,
                                                 xaxis='top',
                                                 xlabel='',
                                                 colorbar=False,
                                                 yaxis='right',
                                                 ylabel='OHSU clusters',
                                                 hooks=[hook]))

    boden_hm.data['20_neighbor'] = boden_hm.data['20_neighbor'].astype(int).astype(str).astype('category')
    boden_hm.data['20_neighbor'].cat.set_categories(boden_corr_order,inplace=True)

    boden_hm.data['marker'] = boden_hm.data['marker'].astype('category')
    boden_hm.data['marker'].cat.set_categories(boden_hm.data['marker'].drop_duplicates().tolist(),inplace=True)
    boden_hm.data = boden_hm.data.sort_values(by=['marker','20_neighbor'])

    boden_hm = hv.HeatMap(boden_hm.data.reset_index(drop=True),kdims=['marker','20_neighbor'],vdims='expression').opts(width=500,
                                                              height=height,
                                                              border=0,
                                                              tools=['hover'],
                                                              colorbar=True,
                                                              colorbar_position='bottom',
                                                              cmap='Spectral_r',
                                                              symmetric=True).redim.range(expression=(-2, 2))

    for dendro in [hv.Empty(),boden_hm_fig.data['top'].opts(width=500,invert_xaxis=False)]:
        boden_hm = boden_hm<<dendro

    final_boden_hm = boden_hm.opts(hv.opts.HeatMap(yrotation=0,
                                                   xrotation=90,
                                                   xaxis='bottom',
                                                   xlabel='',
                                                   colorbar=False,
                                                   yaxis='right',
                                                   ylabel='Basel clusters',
                                                   hooks=[hook]))

    return (comp_bars + final_boden_hm + final_ohsu_hm + hm_fig + corr_box).opts(shared_axes=False).cols(1)


def generate_embeddings(df):
    hv.extension('bokeh')

    df = df[df.metacluster_name!='Segmentation error']
    norm_tsne_coords = pd.read_csv('../TMA/norm_tsne_coords.csv')
    raw_tsne_coords  = pd.read_csv('../TMA/raw_tsne_coords.csv')
    
    raw_tsne_coords['source'] = df.source
    norm_tsne_coords['source'] = df.source

    ck = ['#%02x%02x%02x' % tuple(int(255*j) for j in glasbey_hv[i]) for i in range(180)]

    raw_source_scene = datashade(hv.Scatter(raw_tsne_coords),
                             aggregator=ds.count_cat('source'),
                             color_key=ck).opts(padding=0.1,
                                                xaxis=None,
                                                yaxis=None,
                                                width=700,
                                                height=700)

    norm_source_scene = datashade(hv.Scatter(norm_tsne_coords),
                             aggregator=ds.count_cat('source'),
                             color_key=ck).opts(padding=0.1,
                                                xaxis=None,
                                                yaxis=None,
                                                width=700,
                                                height=700)
    
    suppfig2 = (raw_source_scene + norm_source_scene).opts(shared_axes=False)
    
    norm_tsne_coords['source scene'] = df.source + '_' + df.scene.astype(str)
    norm_tsne_coords['community'] = df['metacluster'].copy()

    ck = ['#%02x%02x%02x' % tuple(int(255*j) for j in glasbey_hv[i]) for i in range(180)]

    source_scene = datashade(hv.Scatter(norm_tsne_coords),
                             aggregator=ds.count_cat('source scene'),
                             color_key=ck).opts(padding=0.1,
                                                xaxis=None,
                                                yaxis=None,
                                                width=700,
                                                height=700)

    ck = [
        '#008000', # B cell/T cell
        '#006400', # B cell
        '#FFFF00', # CD44+ endothelial
        '#808000', 
        '#7CFC00',
        '#556B2F',
        '#00BFFF',
        '#0000CD', # apoptotic
        '#87CEFA',
        '#B0E0E6',
        '#5F9EA0',
        '#CCCC00', # Vimentin+ endothelial
        '#FA8072',
        '#CD5C5C',
        '#FF0000',
        '#800000',
        '#FF8C00',
        '#FFC0CB',
        '#DC143C',
        '#9400D3',
        '#20B2AA',
        '#008080',
        '#DB7093',
        '#CD853F',
        '#FF69B4',
        '#FF1493',
        '#A0522D'
            ]

    community = datashade(hv.Scatter(norm_tsne_coords),
                          aggregator=ds.count_cat('community'),
                          color_key=ck).opts(padding=0.1,
                                             xaxis=None,
                                             yaxis=None,
                                             width=700,
                                             height=700)

    suppfig4 = source_scene + community
    
    return (suppfig2 + suppfig4).opts(shared_axes=False).cols(1)