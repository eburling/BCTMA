# BCTMA

Code for "Toward reproducible, scalable, and robust data analysis acrossmultiplex tissue imaging platforms".

To run notebooks `pyviz_figs.ipynb` and `rapids015_figs.ipynb`, you must first create the necessary conda environments by running:

```
~$ conda create --name myenv --file pyviz-spec.txt
```

and

```
~$ conda create --name myenv --file rapids015-spec.txt
```

Batch normalization of raw CyCIF intensity measurements for TMAs can be achieved on a slurm cluster by running:

```
%%sbatch
#!/usr/bin/bash
#SBATCH --job-name TMA_thresh
#SBATCH --partition exacloud
#SBATCH --get-user-env
#SBATCH --time 36:00:00
#SBATCH --mem 100G
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 4
#SBATCH --array=0-37

eval "$(conda shell.bash hook)"
conda activate pyviz

cd <path to directory containing TMA_RESTORE.py>

srun python TMA_RESTORE.py \
    --source <"name of TMA, e.g. BR1201a-SG48"> \
    --pos_idx $SLURM_ARRAY_TASK_ID \
    --save_dir <"path to desired save directory">
```
